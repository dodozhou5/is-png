# is-png

## 简介
is-png是一个判断当前图片是否是PNG格式的库，他会根据图片的文件数据，判断该图片是否为png格式。

![img.png](img.png)

## 下载安装
```shell
npm install is-png
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

1. 导入isPng
```  
import {isPng} from 'is-png';
```
2. 判断图片格式
   ```  
   private aboutToAppear() {  
        resourceManager.getResourceManager().then(manager=>{
      manager.getMedia($r("app.media.fixture").id).then(value =>{
        this.isPng= isPng(value);
        console.info("image png:"+this.isPng);
      })
    })
    resourceManager.getResourceManager().then(manager=>{
      manager.getMedia($r("app.media.fixtur").id).then(value =>{
        this.isPng2= isPng(value);
        console.info("image png:"+this.isPng2);
      })
    })
   }
   ```
## 接口说明
```
 图片是否为png格式  isPng(buffer)
```

## 兼容性
支持 OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- is-png
|     |---- entry  # 示例代码文件夹 
|     |----README.md  # 安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/is-png/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/is-png/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/is-png/blob/master/LICENSE) ，请自由地享受和参与开源。
